from flask import Flask
from flask_restplus import Resource, Api

from model import api

app = Flask(__name__)
api.init_app(app)

@app.route('/')
def hello_world():
    return 'Hello, World!'

if __name__ == '__main__':
    app.run(debug=True) 